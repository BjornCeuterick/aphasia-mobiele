package com.ucll.bbrk.aphasiaassistantapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText nameEdit;
    Button b1;
    Button b2;
    Button b3;
    Button setNameButton;
    String patientName;
    boolean nameSet;
    String timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button) findViewById(R.id.button3);
        nameEdit = (EditText) findViewById(R.id.nameEdit);
        setNameButton = (Button) findViewById(R.id.nameButton);
        requestStoragePermission();

        nameEdit.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        setNameButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        b1.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        b2.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        b3.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                Intent i = new Intent(this, AboutActivity.class);
                startActivity(i);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void testOneClick(View v){
        Intent i = new Intent(this, TestEen.class);

        timeStamp = new SimpleDateFormat("yyyy.MM.dd---HH.mm").format(new java.util.Date());
        createDir("Aphasia/"+patientName+"/Test1/recordings--"+timeStamp);

        i.putExtra("patientName",patientName);
        i.putExtra("timeStamp",timeStamp);
        i.putExtra("language",getLanguage());

        startActivity(i);
    }

    public void testTwoClick(View v){
        Intent i = new Intent(this, TestTwee.class);
        i.putExtra("patientName",patientName);
        i.putExtra("language",getLanguage());

        startActivity(i);
    }

    public void testThreeClick(View v){
        Intent i = new Intent(this, TestDrie.class);
        i.putExtra("patientName",patientName);
        i.putExtra("language",getLanguage());

        startActivity(i);
    }

    private String getLanguage(){
        String language = Locale.getDefault().getLanguage();
        if(!(language.equals("fr") || language.equals("de") || language.equals("en"))){
            return "en";
        }
        else{
            return language;
        }
    }

    public void setPatientName(View v){
        if(!nameEdit.getText().toString().equals("") && !nameEdit.getText().toString().equals("images")) {
            patientName = nameEdit.getText().toString();
            nameEdit.setEnabled(false);
            setNameButton.setEnabled(false);
            b1.setEnabled(true);
            b2.setEnabled(true);
            b3.setEnabled(true);
            nameSet = true;


            //create app directories
            createDir("Aphasia");
            createDir("Aphasia/"+patientName);
            createDir("Aphasia/"+patientName+"/Test1");
            createDir("Aphasia/"+patientName+"/Test2");
            createDir("Aphasia/"+patientName+"/Test3");
            createDir("Aphasia/images");
            createDir("Aphasia/images/en");
            createDir("Aphasia/images/fr");
            createDir("Aphasia/images/de");
        }
    }

    private void createDir(String path){

        File mydir = new File(Environment.getExternalStorageDirectory() + "/" + path +"/");
        if(!mydir.exists()) {
            mydir.mkdirs();
            Log.d("success", "dir. created");
        }else {
            Log.d("error", "dir. already exists");
        }
    }


    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 23);
    }


    public void onResume(){
        super.onResume();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        nameSet = pref.getBoolean("nameSet",false);

        if(!nameSet || nameEdit.getText().toString().equals("")) {
            nameEdit.setEnabled(true);
            setNameButton.setEnabled(true);
            b1.setEnabled(false);
            b2.setEnabled(false);
            b3.setEnabled(false);
        }
        else{
            nameEdit.setEnabled(false);
            setNameButton.setEnabled(false);
            b1.setEnabled(true);
            b2.setEnabled(true);
            b3.setEnabled(true);
        }
    }

    public void onPause(){
        super.onPause();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean("nameSet", nameSet);
        edit.commit();
    }
}
