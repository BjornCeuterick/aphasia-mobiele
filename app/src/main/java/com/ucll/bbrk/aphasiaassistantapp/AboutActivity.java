package com.ucll.bbrk.aphasiaassistantapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class AboutActivity extends Activity {

    private Button back;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        back = (Button)findViewById(R.id.backButton);
        textView = (TextView) findViewById(R.id.aboutText);

        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,80);
        back.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);

        back.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){finish();}});
    }
}
