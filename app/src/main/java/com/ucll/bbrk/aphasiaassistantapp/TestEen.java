package com.ucll.bbrk.aphasiaassistantapp;

        import android.Manifest;
        import android.app.Activity;
        import android.content.SharedPreferences;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.preference.PreferenceManager;
        import android.support.v4.app.ActivityCompat;
        import android.util.TypedValue;
        import android.widget.ImageView;
        import android.os.Bundle;
        import android.os.Environment;
        import android.widget.Button;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.util.Log;
        import android.media.MediaRecorder;
        import android.widget.TextView;
        import android.widget.Toast;
        import java.io.File;
        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;


public class TestEen extends Activity implements ILoadImages {

    private int imageCounter;
    private ImageView myImage;
    private boolean mStartRecording;
    private List<Bitmap> imageList;
    private Button mRecordButton;
    private MediaRecorder mRecorder=null;
    private Button nextButton;
    private String recordingName;
    private Map<Bitmap, String> imageFileNameMap;
    String patientName;
    String timeStamp;
    String language;


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.test_een);
        requestRecorderPermission();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            patientName = (String)extras.get("patientName");
            timeStamp = (String)extras.get("timeStamp");
            language = (String)extras.get("language");

        }
        else{
            patientName = "invalid";
            timeStamp = "invalid";
            language = "en";
        }

        //counter to select images, 0 = the first
        this.imageCounter = 0;

        myImage = (ImageView) findViewById(R.id.imageView);
        //set the first image
        //load images in a list (imageList) and a hashmap (imageFileMap)
        if(loadImages()) {
            myImage.setImageBitmap(imageList.get(imageCounter));
        }
        else{
            Toast.makeText(this, getResources().getString(R.string.noImagesFound), Toast.LENGTH_LONG).show();
            finish();
        }
        this.nextButton = (Button) findViewById(R.id.next);
        this.nextButton.setEnabled(false);
        this.nextButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);

        //boolean to decide if the recorder has to be stopped or started (used in method onRecord)
        this.mStartRecording = true;
        this.mRecordButton = (Button) findViewById(R.id.mRecordButton);
        mRecordButton.setText(getResources().getString(R.string.startRecording));
        mRecordButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        mRecordButton.setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {
                        onRecord(mStartRecording);
                        mStartRecording = !mStartRecording;
                    }
                });

        this.nextButton = (Button) findViewById(R.id.next);
        //set the nextButton to false, this button is enabled in stopRecording
        nextButton.setEnabled(false);
        nextButton.setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {
                        //this button sets the next image
                        if (imageCounter < imageList.size()) {

                            if (imageCounter == imageList.size()-1){
                                setContentView(R.layout.finished_screen);
                                Button backButton = (Button)findViewById(R.id.backButton);
                                TextView succestext = (TextView)findViewById(R.id.succestext);
                                backButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
                                succestext.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
                                backButton.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
                            }
                            else {
                                imageCounter += 1;
                                myImage.setImageBitmap(imageList.get(imageCounter));
                                nextButton.setEnabled(false);
                                mRecordButton.setEnabled(true);
                            }
                        }
                    }
                }
        );
    }


    private void requestRecorderPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 23);
    }

    @Override
    public void onResume(){
        super.onResume();
    //    Log.d("3", "onresume");
        // if (preferences !=null){
      //  if (this.mRecorder != null){
        //    mRecorder.release();
          //  mRecorder = null;
        //}
        //this.mRecordButton.setEnabled(preferences.getBoolean("recordButtonState", true));
        //Log.d("recordbuttonstate", mRecordButton.isEnabled() +"");
        //this.mRecordButton.setText(preferences.getString("recordButtonText", "default"));
        //this.nextButton.setEnabled(preferences.getBoolean("nextButtonState", true));
        //this.myImage.setImageBitmap(imageList.get(preferences.getInt("imageCounter", 0)));
        //this.mStartRecording = preferences.getBoolean("mStartRecording", true);
        //Log.d("mstartrecording", mRecordButton.isEnabled() +"");
        //   }

    }

    @Override
    public void onPause() {
        super.onPause();
       // Log.d("4 Ik kom in de onpause", "onpause");
        //editor = preferences.edit();
        //editor.putInt("imageCounter", this.imageCounter).apply();
        //editor.putBoolean("recordButtonState", this.mRecordButton.isEnabled()).apply();
        //editor.putBoolean("nextButtonState", this.nextButton.isEnabled()).apply();
        //editor.putString("recordButtonText", this.mRecordButton.getText().toString()).apply();
        //editor.putBoolean("mStartRecording", this.mStartRecording).apply();
        //if (mRecorder != null) {

          //  mRecorder.release();
            //mRecorder = null;
            //stopRecording();
        //}

        // editor.putString("Button",this.startButton.getText().toString());
        //editor.commit();

//        if (mPlayer != null) {
        //          mPlayer.release();
        //        mPlayer = null;
        //  }
    }


    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }


    private void startRecording() {

        this.mRecordButton.setText(getResources().getString(R.string.stopRecording));
        this.recordingName= imageFileNameMap.get(imageList.get(imageCounter));
        this.recordingName = recordingName.substring(0,recordingName.length()-4);

        String fileName =Environment.getExternalStorageDirectory().getAbsolutePath() + "/Aphasia/"+patientName+"/Test1/recordings--"+timeStamp+"/" + recordingName +".3gp";

        File file = new File(fileName);
        if(file.exists()){
            file.delete();
        }

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(fileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();

        } catch (IOException e) {;
            e.printStackTrace();
        }

        mRecorder.start();

    }

    private void stopRecording() {
        this.mRecordButton.setText(getResources().getString(R.string.startRecording));
        this.mRecordButton.setEnabled(false);
        this.nextButton.setEnabled(true);
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        if(imageCounter == imageList.size()-1){
            nextButton.setText(getResources().getString(R.string.finish));
        }
    }


    //initialize the array with images and the hashmap with bitmaps and filenames
    public boolean loadImages() {
        imageList = new ArrayList<Bitmap>();
        imageFileNameMap = new HashMap<Bitmap, String>();

        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Aphasia/images/"+language);

        if (dir.isDirectory()) {

            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; ++i) {
                File f = fileList[i];
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                imageFileNameMap.put(myBitmap, f.getName());
                imageList.add(myBitmap);

                Collections.shuffle(imageList);
            }
        }
        return imageList.size() != 0;
    }

}



