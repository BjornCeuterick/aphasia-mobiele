package com.ucll.bbrk.aphasiaassistantapp;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;


public class TestTwee extends AppCompatActivity implements ILoadImages {
    private ArrayList<String> list;
    TextToSpeech t1;
    private ImageButton buttonOne;
    private ImageButton buttonTwo;
    private ImageButton buttonThree;
    private ImageButton buttonFour;
    private Button next;
    private Button sound;
    private String need;
    private String given;
    List<String> chosenWords;
    ArrayList<String> usedWords;
    private int count;
    String patientName;
    String language;
    Boolean continueLock;

    private Map<String, Bitmap> imageFileNameMap;
    private List<ImageButton> fourButtonsToSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_twee);

        chosenWords = new ArrayList<>();
        usedWords = new ArrayList<>();
        fourButtonsToSelect = new ArrayList<>();


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            patientName = (String)extras.get("patientName");
            language = (String)extras.get("language");
        }
        else{
            patientName = "invalid";
            language = "en";
        }

        continueLock = loadImages();
        if(!continueLock){
            Toast.makeText(this, getResources().getString(R.string.noImagesFound), Toast.LENGTH_LONG).show();
        }



        this.count = 0;
        buttonOne = (ImageButton)findViewById(R.id.buttoneen);
        buttonOne.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){
            handleButtonClick(buttonOne);}});
        buttonTwo = (ImageButton)findViewById(R.id.buttontwee);
        buttonTwo.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){
            handleButtonClick(buttonTwo);}});
        buttonThree = (ImageButton)findViewById(R.id.buttondrie);
        buttonThree.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){
            handleButtonClick(buttonThree);}});
        buttonFour = (ImageButton)findViewById(R.id.buttonvier);
        buttonFour.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){
            handleButtonClick(buttonFour);}});
        next = (Button)findViewById(R.id.nextbutton);
        next.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
        next.setOnClickListener(new View.OnClickListener(){ public void onClick(View v){goNext();}});


        fourButtonsToSelect.add(buttonOne);
        fourButtonsToSelect.add(buttonTwo);
        fourButtonsToSelect.add(buttonThree);
        fourButtonsToSelect.add(buttonFour);

        if(this.getIntent().getStringExtra("given") != null && !this.getIntent().getStringExtra("given").equals("")){
            next.setEnabled(true);
        }
        else{
            next.setEnabled(false);
        }

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    if(language.equals("fr")) {
                        t1.setLanguage(Locale.FRENCH);
                    }
                    else if(language.equals("de")){
                        t1.setLanguage(Locale.GERMAN);
                    }
                    else{
                        t1.setLanguage(Locale.ENGLISH);
                    }
                }
            }
        });


        sound = (Button)findViewById(R.id.soundbutton);
        sound.setText(getResources().getString(R.string.playSound));
        sound.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);

        setTest2Enviroment();
    }
    public void setTest2Enviroment() {
        if(continueLock) {
            this.getIntent().removeExtra("given");
            if (this.getIntent().getStringExtra("given") != null && !this.getIntent().getStringExtra("given").equals("")) {
                next.setEnabled(true);
            } else {
                next.setEnabled(false);
            }
            resetColor();
            setChosenWords();
            bindPictureToButton();
            setNeedUsingChosenWords();

            sound.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String toSpeak = need;
                    t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);

                }
            });
        }
        else {
            finish();
        }
    }

    private void setNeedUsingChosenWords(){
        boolean rdy = false;
        while (!rdy) {
            int r = new Random().nextInt(4);
            need = chosenWords.get(r);
            if (!usedWords.contains(need)) {
                usedWords.add(need);
                rdy = true;
            }
        }
    }

    private void bindPictureToButton(){
        for (int i = 0; i < fourButtonsToSelect.size(); i++) {
            fourButtonsToSelect.get(i).setImageBitmap(this.imageFileNameMap.get(chosenWords.get(i)));
        }
    }

    private void setChosenWords(){
        boolean stop = false;
        while (!stop) {
            List copy = new ArrayList();
            for (String s : list) {
                copy.add(s);
            }
            Collections.shuffle(copy);
            chosenWords = copy.subList(0, 4);
            //minimum 1 niet used
            int helpCounter = 0;
            for (String word : chosenWords) {
                if (!usedWords.contains(word)) {
                    helpCounter++;
                }
            }
            if (helpCounter >= 1) {
                stop = true;
            }
        }
    }


    public void handleButtonClick(ImageButton b){
        resetColor();
        setPreColor();
        b.setAlpha(255);
        String wrd = "";
        if(b.equals(buttonOne)){
            wrd = chosenWords.get(0);
        }
        if(b.equals(buttonTwo)){
            wrd = chosenWords.get(1);
        }
        if(b.equals(buttonThree)){
            wrd = chosenWords.get(2);
        }
        if(b.equals(buttonFour)){
            wrd = chosenWords.get(3);
        }
        given = wrd;

        this.getIntent().putExtra("given",given);

        if(this.getIntent().getStringExtra("given") != null && !this.getIntent().getStringExtra("given").equals("")){
            next.setEnabled(true);
        }
        else{
            next.setEnabled(false);
        }
    }

    public void resetColor(){
        buttonOne.setAlpha(255);
        buttonTwo.setAlpha(255);
        buttonThree.setAlpha(255);
        buttonFour.setAlpha(255);
    }
    public void setPreColor(){
        buttonOne.setAlpha(127);
        buttonTwo.setAlpha(127);
        buttonThree.setAlpha(127);
        buttonFour.setAlpha(127);
    }
    public void goNext(){
        saveChosenPicture(false);
        //shows finish screen after ALL images were shown
        if(count == this.list.size()-1){
            setContentView(R.layout.finished_screen);
            Button backButton = (Button)findViewById(R.id.backButton);
            TextView succestext = (TextView)findViewById(R.id.succestext);
            backButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
            succestext.setTextSize(TypedValue.COMPLEX_UNIT_PX,50);
            backButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    saveChosenPicture(true);
                    finish();
                }
            });
        }else{
            count++;
            chosenWords.clear();
            setTest2Enviroment();
        }
    }


    private void saveChosenPicture(boolean isEndFile){
        String sFileName = this.patientName + " result";

        if(!isEndFile) {
            String sBody = "Question: " + need + "\t\tAnswer: " + given + "\n";
            generateNoteOnSD(this, sFileName, sBody);
        }
        else{
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd---HH.mm.ss").format(new java.util.Date());
            String sBody = "\nTest executed on: " + timeStamp + "\n****************************************************\n\n\n";
            generateNoteOnSD(this, sFileName, sBody);
        }
    }

    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Aphasia/"+patientName+"/Test2/");
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, sFileName + ".txt");
            if (!gpxfile.exists()){
                FileWriter writer = new FileWriter(gpxfile);
                writer.append(sBody);
                writer.flush();
                writer.close();
            }
            else {
                //true = append mode
                FileWriter writer = new FileWriter(gpxfile, true);
                writer.append(sBody);
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean loadImages() {
        imageFileNameMap = new HashMap<String, Bitmap>();
        list = new ArrayList<>();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Aphasia/images/" + language);

        if (dir.isDirectory()) {

            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; ++i) {
                File f = fileList[i];
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
                String listInputName = f.getName().substring(0,f.getName().length()-4);
                imageFileNameMap.put(listInputName, myBitmap);
                list.add(listInputName);
            }
        }
        return list.size() != 0;
    }
}
