package com.ucll.bbrk.aphasiaassistantapp;

/**
 * Created by Bjorn on 26/01/2017.
 */

public interface ILoadImages {

    boolean loadImages();
}
